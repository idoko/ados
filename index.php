<?php include ('inc/header.php') ?>
<nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top scrolling-navbar" >
<div class="container">
    <a href="https://adosmint.com" class="navbar-brand">
        <img src="images/Ados-e 3.png" alt="ADOS">
    </a>
    <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#adosResponsive" aria-controls="adosResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="icon-bar top-bar" ></span>
        <span class="icon-bar middle-bar" ></span>
        <span class="icon-bar bottom-bar" ></span>
    </button>
    <div class="collapse navbar-collapse" id="adosResponsive">
        <ul class="navbar-nav smooth-scroll ml-auto">
            <li class="nav-item">
                <a href="https://adosmint.com" class="nav-link">HOME</a>
            </li>
            <li class="nav-item" >
                <a href="https://adosmint.com/about.php" class="nav-link">ABOUT</a>
            </li>
            <li class="nav-item">
                <a href="https://adosmint.com/order.php" class="nav-link">ORDER</a>
            </li>
            <!-- <li class="nav-item">
                <a href="https://adosmint.com/store.php" class="nav-link">STORE</a>
            </li> -->
            <li class="nav-item">
                <a href="https://adosmint.com/contact_us.php" class="nav-link">CONTACT</a>
            </li>
        </ul>
    </div>
</div>
</nav>
<!-- /.navbar -->

<div id="carouselHome" class="carousel slide carousel-fade" data-ride="carousel">
<ul class="carousel-indicators">
<li data-target="#carouselHome" data-slide-to="0" class="active"></li>
<li data-target="#carouselHome" data-slide-to="1"></li>
<li data-target="#carouselHome" data-slide-to="2"></li>
</ul>
<div class="carousel-inner">
<div class="carousel-item carousel-list active">
<img src="images/items.jpg" class="img-fluid" alt="image">
<div class="carousel-caption">
<div class="slider-text-middle-main">
    <div class="slider-text-middle animated fadeInUp">
   <span class="owl-title-big"> Quality and Suitable products from Reliable Suppliers.</span>
    </div>
</div>
</div>
</div>
<div class="carousel-item carousel-list">
<img src="images/mac.jpeg" class="s img-fluid" alt="image">
<div class="carousel-caption">
<div class="slider-text-middle-main">
    <div class="slider-text-middle animated fadeInUp">
        <span class="owl-title-big">order your desired item online for 310/$.</span>
    </div>
</div>
</div>
</div>
<div class="carousel-item carousel-list">
<img src="images/electronics.jpg" class="img-fluid" alt="image">
<div class="carousel-caption">
<div class="slider-text-middle-main">
    <div class="slider-text-middle animated fadeInUp">
   <span class="owl-title-big"> Gadgets of your choice @ <br> Affordable prices.</span>
    </div>
</div>
</div>
</div>
</div>
</div>
<!--container-->
    <div class="container">
    <section>
            <div class="row">
                <div class="col-md-3 col-sm-3 text-center mb10">
                   <i class="fa fa-edit zoom"></i>

                  <p>We take your order ASAP.</p>
                </div>
                <div class="col-md-3 col-sm-3 text-center mb10">
                    <i class="fa fa-cart-arrow-down zoom bg-gold"></i>
                   <p>We do the shopping,<br> You rest.</p>
                </div>
                <div class="col-md-3 col-sm-3 text-center mb10">
                    <i class="fa fa-globe zoom bg-blue"></i>
                    <p>We deliver to you regardless of your geographical area.</p>
                </div>
                <div class="col-md-3 col-sm-3 text-center mb10">
                    <i class="fa fa-support zoom"></i>
                    <p>24/7 supports from us.</p>
                </div>
            </div>
        </section>
        <hr>
     <!-- Section: Testimonials v.4 -->
<section class="text-center my-5">

<!-- Section heading -->
<h4 class="text-center uppercase">What our Customers say</h4>
        <hr class="underline">
  <!--Carousel Wrapper-->
  <div id="multi-item-example" class="carousel testimonial-carousel slide  carousel-multi-item mb-5" data-ride="carousel">
    <!--Slides-->
    <div class="carousel-inner" role="button">
        <div class="row">
           <div class="col-md-4"></div>
           <div class="col-md-4">
                <!--First slide-->
      <div class="carousel-item active mt-4">
           <!-- Person name -->
           <h4 class="font-weight-bold mt-4">Ani</h4>
          <p class="font-weight-normal"><i class="fa fa-quote-left"></i> It was an awesome experience ordering from amazon.  I really appreciate your loyalty and honesty.</p>
      </div>
      <!-- Second slide -->
      <div class="carousel-item">
          <!-- Person name -->
          <h4 class="font-weight-bold mt-4">ATOM</h4>
          <p class="font-weight-normal"><i class="fa fa-quote-left"></i>Adosmint is great. Bought my iphone X @310/$. #70000 lesser to the price here in Nigeria. </p>
      </div>
      <!-- End of second slide -->
      <!-- Third slide -->
      <div class="carousel-item">
          <!-- Person name -->
          <h4 class="font-weight-bold mt-4">Esther</h4>
          <p class="font-weight-normal"><i class="fa fa-quote-left"></i>One word....WOW!.I bought my Macbook air for a lesser price.I had a marvellous treat from Moyin.</p>
      </div>
      <!-- End of third slide -->
      <!-- Fourth slide -->
      <div class="carousel-item">
          <!-- Person name -->
          <h4 class="font-weight-bold mt-4">John</h4>
          <p class="font-weight-normal"><i class="fa fa-quote-left"></i>Great! Ayo came to my office to market their available shoes,I got mine from him.Indeed quality.</p>
      </div>
      <!-- End of fourth slide -->
           </div>
           <div class="col-md-4"></div>
        </div>
      </div>
      <!-- Left and Right controls -->
      <a href="#multi-item-example" class="carousel-control-prev" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
      </a>
      <a href="#multi-item-example" class="carousel-control-next" role="button" data-slide="next">
          <span class="carosuel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
      </a>
</div>
</section>
<hr>
    <section>
    <h2 class="uppercase text-center mb-5">Order Process</h2>
        <!-- first row -->
       <div class="row">
           <div class="col-md-6">
               <img src="images/process1.png" alt="image" class="img-fluid z-depth-1-half">
           </div>
           <div class="col-md-6">
               <h3 class="text-center mt-4 wow animated bounceIn">Step 1</h3>
               <p class="text-center animated  bounceIn">Browse www.amazon.com</p>
           </div>
       </div>
       <hr class="my-5">
       <!-- second row -->
       <div class="row mt-5">
           <div class="col-md-6 animated bounceInUp">
               <h3 class="text-center">Step 2</h3>
               <p class="text-center">Search for your desired item</p>
               <p></p>
           </div>
           <div class="col-md-6">
               <img src="images/process2.png" alt="image" class="img-fluid z-depth-1-half">
           </div>
       </div>
       <hr class="my-5">
       <!-- third row -->
       <div class="row mt-5">
           <div class="col-md-6">
               <img src="images/process3.png" alt="image" class="img-fluid z-depth-1-half">
           </div>
           <div class="col-md-6 animated fadeIn">
               <h3 class="text-center mt-4">Step 3</h3>
               <p class="text-center">Copy the address link in the <strong>RED CIRCLED BOX</strong> of the product from the URL </p>
           </div>
       </div>
       <hr class="my-5">
       <!-- fourth row -->
       <div class="row mt-5">
           <div class="col-md-6">
               <h3 class="text-center animated fadeIn">Step 4</h3>
               <p class="text-center">Paste <strong>COPIED LINK</strong> in the <strong> RED CYCLED BOX</strong> above in ORDER page as shown in the image</p>
           </div>
           <div class="col-md-6">
               <img src="images/process4.png" alt="image" class="img-fluid z-depth-1-half">
           </div>
       </div>
    </section>
    </div>
   <?php include ('inc/footer.php'); ?>