<?php
ob_clean();
ob_start();
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="google-site-verification" content="n5a0qKsj5TdWJbydz3kbCm78PgWxsAmXwx2ATRGiK0A" />
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="author" content="Afolabi Moyinoluwa">
    <title>Adosmint - No 1 shopping agency in Nigeria.</title>
    <!-- Logo icon -->
    <link rel="shortcut icon" href="images/favicon.ico">
    <!-- Font Awesome -->
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap core css -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <!-- MDB -->
  <link href="css/mdb.min.css" rel="stylesheet">
  <!-- Animate core css -->
    <link rel="stylesheet" href="css/animate.css">
    <!-- Custom css -->
    <link rel="stylesheet" href="css/theme.css">
    <!-- Google fonts -->  
<link href="https://fonts.googleapis.com/css?family=Arapey|Armata|Open+Sans+Condensed:300" rel="stylesheet">
<!-- Start of Async Drift Code -->
<script>
"use strict";

!function() {
  var t = window.driftt = window.drift = window.driftt || [];
  if (!t.init) {
    if (t.invoked) return void (window.console && console.error && console.error("Drift snippet included twice."));
    t.invoked = !0, t.methods = [ "identify", "config", "track", "reset", "debug", "show", "ping", "page", "hide", "off", "on" ], 
    t.factory = function(e) {
      return function() {
        var n = Array.prototype.slice.call(arguments);
        return n.unshift(e), t.push(n), t;
      };
    }, t.methods.forEach(function(e) {
      t[e] = t.factory(e);
    }), t.load = function(t) {
      var e = 3e5, n = Math.ceil(new Date() / e) * e, o = document.createElement("script");
      o.type = "text/javascript", o.async = !0, o.crossorigin = "anonymous", o.src = "https://js.driftt.com/include/" + n + "/" + t + ".js";
      var i = document.getElementsByTagName("script")[0];
      i.parentNode.insertBefore(o, i);
    };
  }
}();
drift.SNIPPET_VERSION = '0.3.1';
drift.load('57i95fzd2s8i');
</script>
<!-- End of Async Drift Code -->
</head>
<body>