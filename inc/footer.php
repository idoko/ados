<footer class="page-footer font-small unique-color-dark pt-0">
        <!-- social plugins -->
        <div class="primary-color">
            <div class="container">
                <!-- row -->
                <div class="row py-4 d-flex align-items-center">
                    <!-- column -->
                    <div class="col-md-6 col-lg-5 text-center text-md-left mb-4 mb-md-0">
                        <h6 class="mb-0 white-text">Get connected with us on social networks</h6>
                    </div>
                    <!-- end column -->
                    <!-- column -->
    <div class="col-md-6 col-lg-7 text-center text-md-right">
        <!--Facebook-->
    <a class="fb-ic ml-0" href="#">
        <i class="fa fa-facebook white-text mr-4"> </i>
    </a>
        <!--Twitter-->
    <a class="tw-ic" href="https://api.whatsapp.com/send?phone=2348104383073">
        <i class="fa fa-whatsapp white-text mr-4"> </i>
    </a>
        <!--Instagram-->
    <a class="ins-ic" href="https://instagram.com/adosenterprises">
        <i class="fa fa-instagram white-text mr-lg-4"> </i>
    </a>
                    </div>
                </div>
                <!-- end row -->
            </div>
            <!-- end container -->
        </div>
        <!-- end social plugins -->
        <div class="container mt-5 text-center text-md-left">
            <div class="row mt-3">
                    <div class="col-md-4 col-lg-4">
                        <img src="images/Ados-e 3.png" alt="ADOS"  class="logo">
                    </div>
    <div class=" col-md-4 col-lg-4">
            <div class="widget">
                <h6 class="title">LATEST UPDATES</h6>
                <hr class="hr">
        <ul class="link-list list-unstyled latest-updates">
            <li>
                <a href="#">Check the latest Iphone XS</a>
                    <span class="date">September
                            <span class="number">26,2018</span>
                    </span>
            </li>
            <li>
                <a href="https://instagram.com/adosenterprises">Check this beautiful amazing gucci bag <br> on our Instagram post</a>
                    <span class="date">August
                        <span class="number">18,2018</span>
                    </span>
            </li>
        </ul>
            </div>
        <!-- END OF WIDGET -->
    </div>

    <div class="col-md-4 col-lg-4 mb-5">
    &copy; copyright 2018
      <br>
      <i class="fa fa-map-marker"></i> Omole Estate,Mayfair,Ile-Ife. | Adosmint.com - All Rights Reserved.
    </div>
<!-- END OF COL -->
            </div>
        </div>
<!-- END OF CONTAINER -->
    </footer>
    <!-- JQUERY -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<!-- Bootstrap tooltips -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<!-- VALIDATE JS - JQUERY PLUGIN -->
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery@3.2.1/dist/jquery.min.js"></script>
<!-- Bootstrap Javascript-->
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <!-- MDB JavaScript -->
  <script type="text/javascript" src="js/mdb.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.9/validator.min.js" integrity="sha256-dHf/YjH1A4tewEsKUSmNnV05DDbfGN3g7NMq86xgGh8="
        crossorigin="anonymous"></script>
        <!-- CUSTOM JAVSCRIPT -->
    <script type="text/javascript" src="js/ados.js" ></script>
</body>
</html>